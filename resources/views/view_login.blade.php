<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
@if (Session::has('error'))
	<h1 style="color: red">
		{{Session::get('error')}}
	</h1>
@endif
@if (Session::has('success'))
	<h1 style="color: green">
		{{Session::get('success')}}
	</h1>
@endif
<form method="post" action="{{ route('process_login') }}">
	{{csrf_field()}}
	Email
	<input type="email" name="email">
	<br>
	Mat khau
	<input type="password" name="mat_khau">
	<br>
	<button>Dang nhap</button>
</form>
</body>
</html>
<form action="{{ route('sinh_vien.store') }}" method="post">
	{{csrf_field()}}
	Ten
	<input type="text" name="ten">
	<br>
	Ngay Sinh
	<input type="date" name="ngay_sinh">
	<br>
	Chọn lớp
	<select name="ma_lop">
		@foreach ($array_lop as $lop)
			<option value="{{$lop->ma}}"
			@if ($lop->ma == $ma_lop)
				selected 
			@endif
			>
				{{$lop->ten}}
			</option>
		@endforeach
	</select>
	<br>
	<button>Them</button>
</form>
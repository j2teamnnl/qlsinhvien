<?php


Route::get('','LoginController@view_login')->name('view_login');
Route::post('process_login','LoginController@process_login')->name('process_login');

Route::group(['middleware' => 'CheckLogin'],function(){
	Route::get('welcome','Controller@welcome')->name('welcome');


	Route::group(['as' => 'sinh_vien.', 'prefix' => 'sinh_vien'],function(){
		Route::get('choose_by_lop','SinhVienController@choose_by_lop')->name('choose_by_lop');
		Route::get('view_by_lop','SinhVienController@view_by_lop')->name('view_by_lop');

		// đổi ảnh
		Route::get('view_change_anh/{ma}','SinhVienController@view_change_anh')->name('view_change_anh');
		Route::post('process_change_anh/{ma}','SinhVienController@process_change_anh')->name('process_change_anh');

		// làm việc với file excel
		Route::get('view_insert_with_excel','SinhVienController@view_insert_with_excel')->name('view_insert_with_excel');
		Route::post('process_insert_with_excel','SinhVienController@process_insert_with_excel')->name('process_insert_with_excel');

		Route::get('export_excel_from_view_all','SinhVienController@export_excel_from_view_all')->name('export_excel_from_view_all');
	});
	Route::resource('sinh_vien','SinhVienController');




	Route::resource('lop','LopController');

	Route::get('logout','LoginController@logout')->name('logout');
});

Route::get('send_mail','Controller@send_mail');


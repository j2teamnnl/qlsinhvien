<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
use Exception;

class LoginController extends Controller
{
	function view_login(){
		return view('view_login');
	}
	public function process_login(Request $rq)
	{
		$email = $rq->get('email');
		$mat_khau = $rq->get('mat_khau');
		try {
			$admin = Admin::where('email',$email)
			->where('mat_khau',$mat_khau)
			->firstOrFail();

			$rq->session()->put('ma',$admin->ma);
			$rq->session()->put('ten',$admin->ten);

			return redirect()->route('welcome');
		} catch (Exception $e) {
			return redirect()->route('view_login')->with('error','Dang nhap sai roi');
		}
		
	}
	public function logout(Request $rq)
	{
		$rq->session()->flush();

		return redirect()->route('view_login')->with('success','Dang xuat sai roi');
	}
}
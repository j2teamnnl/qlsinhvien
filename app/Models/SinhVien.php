<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DateTime;

class SinhVien extends Model
{
    protected $table = 'sinh_vien';
    protected $fillable = ['ten','ngay_sinh','gioi_tinh','anh','ma_lop'];
    protected $primaryKey = 'ma';

    public $timestamps = false;
    public function lop()
    {
        return $this->belongsTo('App\Models\Lop','ma_lop');
    }

    public function getTuoiAttribute()
    {
		$age = date_diff(date_create($this->ngay_sinh), date_create('now'))->y;
	    return $age;
    }
    public function getTenGioiTinhAttribute()
    {
    	if($this->gioi_tinh==1){
    		return "Nu";
    	}
    	else{
    		return "Nam";
    	}
    }
}
